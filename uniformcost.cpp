#include "uniformcost.h"

#include <QDebug>

UniformCost::UniformCost(Map* map, bool m, std::vector<char> directions, bool repeat_nodes, Tree* tree) :
  IA(map, m, directions, repeat_nodes, tree) {
    frontier.push_back(std::pair<QPoint, float>(map->start, 0));
    tree->insertAlgorithm(map->start, map->start, DataNode());
}

std::pair<QPoint, float> UniformCost::min() {
    unsigned i = 0, j = 0;
    for (auto pair : frontier) {
        if (pair.second < frontier[i].second) {
            i = j;
        }
        ++j;
    }
    auto p = frontier[i];
    frontier.erase(frontier.begin() + i);
    return p;
}

int UniformCost::find(QPoint a) {
    for (unsigned i = 0; i < frontier.size(); ++i) {
        if (frontier[i].first == a) {
            return static_cast<int>(i);
        }
    }
    return -1;
}

bool UniformCost::step() {
    if (not frontier.size()) {
        throw std::logic_error("Can't reach end");
    }
    auto c = min();
    current = c.first;
    if (current == map->end) {
        return true;
    }
    closed.push_back(current);
    DataNode data;
    for (auto neigbor : map->neighbors(current, directions)) {
        if (repeat_nodes or std::find(closed.begin(), closed.end(), neigbor) == closed.end()) {
            auto aux = find(neigbor);
            if (aux == -1) {
                frontier.push_back(std::pair<QPoint, float>(neigbor, c.second + map->at(neigbor)));
                data.gn = c.second + map->at(neigbor);
                data.visit = closed.size();
                tree->insertAlgorithm(neigbor, current, data);
            } else if (c.second + map->at(neigbor) < frontier[static_cast<unsigned>(aux)].second) {
                frontier[static_cast<unsigned>(aux)].second = c.second + map->at(neigbor);
                data.gn = c.second + map->at(neigbor);
                tree->changeNode(neigbor, current, data);
            }
        }
    }
    return false;
}
