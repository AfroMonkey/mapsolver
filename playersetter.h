#ifndef PLAYERSETTER_H
#define PLAYERSETTER_H

#include "area.h"
#include "player.h"

#include <QDialog>
#include <QWidget>

namespace Ui {
    class PlayerSetter;
}

class PlayerSetter : public QDialog {
    Q_OBJECT

public:
    explicit PlayerSetter(QWidget* parent = nullptr, std::vector<Player>* players = nullptr, std::vector<Area>* areas = nullptr);
    ~PlayerSetter();

private slots:
    void done(int r);
    void on_pushButton_clicked();

private:
    Ui::PlayerSetter* ui;
    std::vector<Player>* players;
};

#endif // PLAYERSETTER_H
