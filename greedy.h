#ifndef GREEDY_H
#define GREEDY_H

#include "ia.h"

class Greedy : public IA {
private:
    std::vector<QPoint> q;
    std::vector<QPoint> closed;
    QPoint min();

public:
    Greedy(Map* map, bool m, std::vector<char> directions, bool repeat_nodes, Tree* tree);
    bool step();
};

#endif // GREEDY_H
