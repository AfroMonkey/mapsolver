#ifndef TREE_H
#define TREE_H

#include "area.h"
#include "datanode.h"
#include "map.h"
#include "player.h"

class Tree {
public:
    Tree();

    ptNode root;
    bool ia;
    void insert(DataNode d, ptNode f);
    ptNode findNode(int x, int y, ptNode node);
    void insertChildren(int x, int y, Player* player, Map map, std::vector<Area> areas);
    void insertAlgorithm(QPoint point, QPoint father, DataNode data);
    void insertForAlgorithm(QPoint point, ptNode f, DataNode data);
    void changeNode(QPoint point, QPoint newFather, DataNode newData);
};

#endif // TREE_H
