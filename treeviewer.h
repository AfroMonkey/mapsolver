#ifndef TREEVIEWER_H
#define TREEVIEWER_H

#include "tree.h"

#include <QDialog>
#include <QGraphicsTextItem>
#include <QTreeWidget>
#include <QWidget>
#include <QtCore>
#include <QtGui>

namespace Ui {
    class TreeViewer;
}

class TreeViewer : public QDialog {
    Q_OBJECT

public:
    explicit TreeViewer(QWidget* parent = nullptr, ptNode root = nullptr, std::vector<std::vector<QGraphicsTextItem*>> map_turns = {});
    ~TreeViewer();

    void convertTree(ptNode root, std::vector<std::vector<QGraphicsTextItem*>> map_turns);
    void addRoot(QTreeWidgetItem* item);
    void addChild(QTreeWidgetItem* parent, QTreeWidgetItem* item);
    void convertChildren(ptNode node, QTreeWidgetItem* parent, std::vector<std::vector<QGraphicsTextItem*>> map_turns);

private:
    Ui::TreeViewer* ui;
    ptNode root;
    std::vector<std::vector<QGraphicsTextItem*>> map_turns;
};

#endif // TREEVIEWER_H
