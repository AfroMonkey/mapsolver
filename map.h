#ifndef MAP_H
#define MAP_H

#include "player.h"

#include <QPoint>
#include <vector>

#define MAX_COLS 15
#define MAX_ROWS 15

class Map {
public:
    int cols;
    int rows;
    int map[MAX_ROWS][MAX_COLS];
    bool masked[MAX_ROWS][MAX_COLS];
    QPoint start;
    QPoint end;
    Player* player;

    Map();
    void clear();
    void unmask(int x, int y);
    void mask_all();
    int* operator[](int y) { return map[y]; }
    std::vector<QPoint> neighbors(QPoint cell, std::vector<char> directions);
    float at(QPoint a);
};

#endif // MAP_H
