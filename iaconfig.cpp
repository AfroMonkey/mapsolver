#include "iaconfig.h"

#include "ui_iaconfig.h"

#include <QDebug>

IAConfig::IAConfig(QWidget* parent, std::vector<char>* directions_remote, bool* repeat_nodes, bool* manhattan, char* algorithm) :
  QDialog(parent), ui(new Ui::IAConfig), directions_remote(directions_remote),
  repeat_nodes(repeat_nodes),
  manhattan(manhattan),
  algorithm(algorithm) {
    ui->setupUi(this);
    this->directions = *directions_remote;
    for (unsigned long p = 0; p < 4; p++) {
        switch (directions[p]) {
            case 'u':
                ui->up->setText(QString::number(p));
                break;
            case 'r':
                ui->right->setText(QString::number(p));
                break;
            case 'd':
                ui->down->setText(QString::number(p));
                break;
            case 'l':
                ui->left->setText(QString::number(p));
                break;
        }
    }
    i = 0;
    ui->labelI->setText(QString::number(i));
    ui->repeatNodesCheckBox->setChecked(*repeat_nodes);
    ui->radioButton_Manhattan->setChecked(*manhattan);
    ui->radioButton_Euclidean->setChecked(not(*manhattan));
    ui->algorithmComboBox->addItem("A*");
    ui->algorithmComboBox->addItem("Uniform Cost");
    ui->algorithmComboBox->addItem("Greedy");
    switch (*algorithm) {
        case 'a':
            ui->algorithmComboBox->setCurrentIndex(0);
            break;
        case 'u':
            ui->algorithmComboBox->setCurrentIndex(1);
            break;
        case 'g':
            ui->algorithmComboBox->setCurrentIndex(2);
            break;
    }
}

IAConfig::~IAConfig() {
    delete ui;
}

void IAConfig::bc(QPushButton* button) {
    unsigned long current = static_cast<unsigned long>((button->text()[0] == '&' ? button->text()[1].unicode() : button->text()[0].unicode()) - '0');
    switch (directions[i]) {
        case 'u':
            ui->up->setText(QString::number(current));
            break;
        case 'r':
            ui->right->setText(QString::number(current));
            break;
        case 'd':
            ui->down->setText(QString::number(current));
            break;
        case 'l':
            ui->left->setText(QString::number(current));
            break;
    }
    button->setText(QString::number(i));
    auto aux = directions[current];
    directions[current] = directions[i];
    directions[i] = aux;
    i = (i + 1) % 4;
    ui->labelI->setText(QString::number(i));
}

void IAConfig::on_up_clicked() {
    bc(ui->up);
}

void IAConfig::on_right_clicked() {
    bc(ui->right);
}

void IAConfig::on_down_clicked() {
    bc(ui->down);
}

void IAConfig::on_left_clicked() {
    bc(ui->left);
}

void IAConfig::done(int r) {
    if (QDialog::Accepted == r) {
        *directions_remote = directions;
        *repeat_nodes = ui->repeatNodesCheckBox->isChecked();
        *manhattan = ui->radioButton_Manhattan->isChecked();
        *algorithm = ui->algorithmComboBox->currentText().toLower().toStdString()[0];
    }
    QDialog::done(r);
}
