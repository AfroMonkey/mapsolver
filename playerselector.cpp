#include "playerselector.h"

#include "mainwindow.h"
#include "ui_playerselector.h"

#include <QDebug>

PlayerSelector::PlayerSelector(QWidget* parent, std::vector<Player>* players, Player** player) :
  QDialog(parent), ui(new Ui::PlayerSelector), players(players), player(player) {
    ui->setupUi(this);

    ui->selectTable->setHorizontalHeaderLabels(QStringList({tr("Name")}));

    int row = 0;
    for (auto player : *players) {
        ui->selectTable->insertRow(row);
        ui->selectTable->setItem(row, 0, new QTableWidgetItem(player.name));
        ui->selectTable->item(row, 0)->setFlags(ui->selectTable->item(row, 0)->flags() & ~Qt::ItemIsEditable);
        ++row;
    }
}

PlayerSelector::~PlayerSelector() {
    delete ui;
}

void PlayerSelector::done(int r) {
    if (QDialog::Accepted == r) {
        if (ui->selectTable->selectedItems().empty()) {
            return;
        }
        *player = &(*players)[static_cast<std::size_t>(ui->selectTable->selectedItems()[0]->row())];
    }
    QDialog::done(r);
}
