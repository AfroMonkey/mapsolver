#include "map.h"

#include <cstring>

Map::Map() {
    clear();
}

void Map::clear() {
    cols = rows = 0;
    std::memset(map, -1, MAX_COLS * MAX_ROWS);
    mask_all();
}

void Map::mask_all() {
    std::memset(masked, true, MAX_COLS * MAX_ROWS);
    if (not start.isNull()) {
        masked[start.y()][start.x()] = false;
    }
    if (not end.isNull()) {
        masked[end.y()][end.x()] = false;
    }
}

void Map::unmask(int x, int y) {
    masked[y][x] = false;
    if (x > 0) {
        masked[y][x - 1] = false;
    }
    if (x < MAX_COLS) {
        masked[y][x + 1] = false;
    }
    if (y > 0) {
        masked[y - 1][x] = false;
    }
    if (y < MAX_ROWS) {
        masked[y + 1][x] = false;
    }
}

std::vector<QPoint> Map::neighbors(QPoint cell, std::vector<char> directions) {
    std::vector<QPoint> n;
    for (auto d : directions) {
        QPoint aux(-1, -1);
        switch (d) {
            case 'u':
                if (cell.y() > 0) {
                    aux = QPoint(cell.x(), cell.y() - 1);
                }
                break;
            case 'r':
                if (cell.x() < cols - 1) {
                    aux = QPoint(cell.x() + 1, cell.y());
                }
                break;
            case 'd':
                if (cell.y() < rows - 1) {
                    aux = QPoint(cell.x(), cell.y() + 1);
                }
                break;
            case 'l':
                if (cell.x() > 0) {
                    aux = QPoint(cell.x() - 1, cell.y());
                }
                break;
        }
        if (at(aux) >= 0) {
            n.push_back(aux);
        }
    }
    return n;
}

float Map::at(QPoint a) {
    if (a.x() < 0 or a.y() < 0 or a.x() >= cols or a.y() >= rows) {
        return -1;
    }
    for (auto mc : player->movement_costs) {
        if (mc.first == map[a.y()][a.x()]) {
            return mc.second;
        }
    }
    return -1;
}
