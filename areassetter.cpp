#include "areassetter.h"

#include "mainwindow.h"
#include "ui_areassetter.h"

#include <QColorDialog>
#include <QDebug>
#include <QMessageBox>
#include <QToolButton>

AreasSetter::AreasSetter(QWidget* parent, std::vector<Area>* areas) :
  QDialog(parent), ui(new Ui::AreasSetter), areas(areas) {
    ui->setupUi(this);
    if (not areas) {
        return;
    }

    initialize();
}

void AreasSetter::getColor() {
    QToolButton* color_button = static_cast<QToolButton*>(sender());
    QColor color = QColorDialog::getColor(color_button->icon().pixmap(20, 20).toImage().pixelColor(0, 0));
    QPixmap px(20, 20);
    px.fill(color);
    color_button->setIcon(px);
}

void AreasSetter::initialize() {
    ui->areasTable->setHorizontalHeaderLabels(QStringList({tr("ID"), tr("Color"), tr("Name")}));

    int row = 0;
    for (auto area : *areas) {
        ui->areasTable->insertRow(row);
        // ID
        ui->areasTable->setItem(row, 0, new QTableWidgetItem(QString::number(area.id)));
        ui->areasTable->item(row, 0)->setFlags(ui->areasTable->item(row, 0)->flags() & ~Qt::ItemIsEditable);
        // Color Picker
        ui->areasTable->setItem(row, 1, new QTableWidgetItem(area.color));
        QToolButton* color_button = new QToolButton(this);
        connect(color_button, SIGNAL(clicked()), this, SLOT(getColor()));
        QPixmap px(20, 20);
        px.fill(area.color);
        color_button->setIcon(px);
        ui->areasTable->setCellWidget(row, 1, color_button);
        ui->areasTable->cellWidget(row, 1)->setAccessibleName(color_button->icon().pixmap(20, 20).toImage().pixelColor(0, 0).name());
        ui->areasTable->item(row, 1)->setText(ui->areasTable->cellWidget(row, 1)->accessibleName());
        //Name
        ui->areasTable->setItem(row, 2, new QTableWidgetItem(area.name));
        ++row;
    }
}

AreasSetter::~AreasSetter() {
    delete ui;
}

void AreasSetter::done(int r) {
    if (QDialog::Accepted == r) {
        QStringList names;
        for (unsigned i = 0; i < areas->size(); ++i) {
            QToolButton* color_button = static_cast<QToolButton*>(ui->areasTable->cellWidget(int(i), 1));
            (*areas)[i].color = color_button->icon().pixmap(20, 20).toImage().pixelColor(0, 0).name();
            (*areas)[i].name = ui->areasTable->item(int(i), 2)->text();
            if ((*areas)[i].name.isEmpty()) {
                QMessageBox::critical(this, tr("MapSolver"), QString(tr("Area %1 has no name")).arg((*areas)[i].id), QMessageBox::Ok);
                return;
            }
            if (names.contains((*areas)[i].name)) {
                QMessageBox::critical(this, tr("MapSolver"), QString(tr("Duplicated area name %1")).arg((*areas)[i].name), QMessageBox::Ok);
                return;
            }
            names.append((*areas)[i].name);
        }
    }
    QDialog::done(r);
}
