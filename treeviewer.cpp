#include "datanode.h"
#include "treeviewer.h"
#include "ui_treeviewer.h"

#include <QDialog>
#include <QtCore>
#include <QtGui>

TreeViewer::TreeViewer(QWidget* parent, ptNode root, std::vector<std::vector<QGraphicsTextItem*>> map_turns) :
  QDialog(parent),
  ui(new Ui::TreeViewer) {
    ui->setupUi(this);

    ui->treeWidget->setColumnCount(1);
    convertTree(root, map_turns);
}

TreeViewer::~TreeViewer() {
    delete ui;
}

void TreeViewer::addRoot(QTreeWidgetItem* item) {
    ui->treeWidget->addTopLevelItem(item);
}

void TreeViewer::addChild(QTreeWidgetItem* parent, QTreeWidgetItem* item) {
    parent->addChild(item);
}

void TreeViewer::convertTree(ptNode root, std::vector<std::vector<QGraphicsTextItem*>> map_turns) {
    if (root != nullptr) {
        QTreeWidgetItem* item = new QTreeWidgetItem(ui->treeWidget);
        item->setText(0, (QString('A' + root->data.x) +
                          QString::number(root->data.y + 1) +
                          "  _  0  _  Cerrado  _  " +
                          map_turns[static_cast<std::size_t>(root->data.y)][static_cast<std::size_t>(root->data.x)]->toPlainText()));
        addRoot(item);
        convertChildren(root, item, map_turns);
    }
    ui->treeWidget->expandAll();
}

void TreeViewer::convertChildren(ptNode node, QTreeWidgetItem* parent, std::vector<std::vector<QGraphicsTextItem*>> map_turns) {
    QVector<Node*>::iterator it = node->children.begin();
    for (int i = 0; i < node->children.size(); i++, it++) {
        QTreeWidgetItem* item = new QTreeWidgetItem();
        QString closed;
        if ((*it)->data.closed) {
            closed = "Cerrado";
        } else {
            closed = "Abierto";
        }
        item->setText(0, (QString('A' + (*it)->data.x) +
                          QString::number((*it)->data.y + 1) +
                          "  _  (" + QString().setNum((*it)->data.gn, 'g', 2) +
                          "+" + QString().setNum((*it)->data.hn, 'g', 2) + ") = " +
                          QString().setNum(((*it)->data.gn+(*it)->data.hn), 'g', 2) +
                          "  _  " + closed + "  _  "  +  QString().setNum((*it)->data.visit)));
                          //map_turns[static_cast<std::size_t>((*it)->data.y)][static_cast<std::size_t>((*it)->data.x)]->toPlainText()));
        addChild(parent, item);
        convertChildren(*it, item, map_turns);
    }
}
