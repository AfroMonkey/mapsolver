#include "mainwindow.h"

#include "greedy.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget* parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow) {
    ui->setupUi(this);

    MAP_WIDTH = ui->kanbas->width() - 2;
    MAP_HEIGHT = ui->kanbas->height() - 2;

    start_setting = end_setting = false;
    turn = 0;
    turnLabel = new QLabel();
    ui->statusBar->addPermanentWidget(turnLabel);

    scene = new QGraphicsScene(0, 0, MAP_WIDTH + 1, MAP_HEIGHT + 1);

    rect_start = rect_end = nullptr;
    player_image = nullptr;
    player = nullptr;
    set_flags(0x0000);
    prepare_scene();
    directions = {
      Direction::Up,
      Direction::Right,
      Direction::Down,
      Direction::Left};
    repeat_nodes = false;
    manhattan = true;
    algorithm = 'a';
    ia = nullptr;
}

MainWindow::~MainWindow() {
    clear();
    delete ui;
}

void MainWindow::prepare_labels() {
    QStringList hh;
    for (int i = 0; i < map.cols; ++i) {
        hh << QString('A' + i);
    }
    ui->horizontalLabels->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    ui->horizontalLabels->horizontalHeader()->setMaximumSectionSize(cell_width);
    ui->horizontalLabels->horizontalHeader()->setMinimumSectionSize(cell_width);
    ui->horizontalLabels->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->horizontalLabels->setColumnCount(map.cols);
    ui->horizontalLabels->setHorizontalHeaderLabels(hh);
    QStringList vh;
    for (int i = 1; i <= map.rows; ++i) {
        vh << QString::number(i);
    }
    ui->verticalLabels->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    ui->verticalLabels->verticalHeader()->setMaximumSectionSize(cell_height);
    ui->verticalLabels->verticalHeader()->setMinimumSectionSize(cell_height);
    ui->verticalLabels->verticalHeader()->setDefaultAlignment(Qt::AlignLeft);
    ui->verticalLabels->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->verticalLabels->setRowCount(map.rows);
    ui->verticalLabels->setVerticalHeaderLabels(vh);
}

void MainWindow::prepare_scene() {
    ui->kanbas->setScene(scene);
    ui->kanbas->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->kanbas->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void MainWindow::render_map() {
    cell_width = MAP_WIDTH / map.cols;
    cell_height = MAP_HEIGHT / map.rows;

    prepare_labels();
    for (int r = 0; r < map.rows; ++r) {
        std::vector<QGraphicsRectItem*> row_areas;
        std::vector<QGraphicsRectItem*> row_mask;
        std::vector<QGraphicsRectItem*> row_path;
        std::vector<QGraphicsTextItem*> row_turns;
        for (int c = 0; c < map.cols; ++c) {
            row_areas.push_back(scene->addRect(QRectF(c * cell_width, r * cell_height, cell_width, cell_height), QPen(), QBrush(QColor(get_area(map[r][c]).color))));
            QGraphicsRectItem* mask = scene->addRect(QRectF(c * cell_width, r * cell_height, cell_width, cell_height), QPen(Qt::white), QBrush(Qt::black));
            mask->setVisible(false);
            row_mask.push_back(mask);
            QGraphicsRectItem* path = scene->addRect(QRectF(c * cell_width + 2, r * cell_height + 2, cell_width - 4, cell_height - 4), QPen(QBrush(Qt::black), 4, Qt::DotLine, Qt::SquareCap, Qt::MiterJoin));
            path->setVisible(false);
            row_path.push_back(path);
            QGraphicsTextItem* turn_label = scene->addText("", QFont("monospace", int(cell_width / 7.6)));
            turn_label->setPos(c * cell_width - 2, r * cell_height - 3);
            turn_label->setZValue(3);
            turn_label->setDefaultTextColor(Qt::black);
            row_turns.push_back(turn_label);
        }
        map_areas.push_back(row_areas);
        map_mask.push_back(row_mask);
        map_path.push_back(row_path);
        map_turns.push_back(row_turns);
    }
    if (not rect_start) {
        rect_start = scene->addRect(QRectF(0, 0, cell_width - 6, cell_height - 6), QPen(QBrush(Qt::white), 6, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
    }
    if (not rect_end) {
        rect_end = scene->addRect(QRectF(0, 0, cell_width - 6, cell_height - 6), QPen(QBrush(Qt::red), 6, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
    }
    if (not player_image) {
        player_image = scene->addPixmap(QPixmap());
    }
}

void MainWindow::get_areas() {
    for (int r = 0; r < map.rows; ++r) {
        for (int c = 0; c < map.cols; ++c) {
            if (std::find(areas.begin(), areas.end(), map[r][c]) == areas.end()) {
                areas.push_back(Area(map[r][c]));
            }
        }
    }
    std::sort(areas.begin(), areas.end());
}

void MainWindow::set_flags(unsigned flags) {
    this->flags = flags;
    ui->actionSet_areas->setEnabled(Flag::MapLoaded & flags);
    ui->actionSet_start->setEnabled(Flag::AreasSetted & flags);
    ui->actionSet_end->setEnabled(Flag::AreasSetted & flags);
    ui->actionUp->setEnabled(Flag::PlayerSetted & flags and not(Flag::Win & flags) and not(Flag::PlayingIA & flags));
    ui->actionRight->setEnabled(Flag::PlayerSetted & flags and not(Flag::Win & flags) and not(Flag::PlayingIA & flags));
    ui->actionDown->setEnabled(Flag::PlayerSetted & flags and not(Flag::Win & flags) and not(Flag::PlayingIA & flags));
    ui->actionLeft->setEnabled(Flag::PlayerSetted & flags and not(Flag::Win & flags) and not(Flag::PlayingIA & flags));
    ui->actionLoad_player->setEnabled(Flag::AreasSetted & flags and Flag::StartSetted & flags and Flag::EndSetted & flags);
    ui->actionSelect_player->setEnabled(Flag::PlayerCreated & flags);
    if (rect_start) {
        rect_start->setVisible(Flag::StartSetted & flags);
    }
    if (rect_end) {
        rect_end->setVisible(Flag::EndSetted & flags);
    }
    if (player_image) {
        player_image->setVisible(Flag::PlayerSetted & flags);
    }
    ui->actionIA->setEnabled(Flag::PlayerSetted & flags and not(Flag::Win & flags) and not(Flag::PlayingUser & flags));
}

Area MainWindow::get_area(int id) {
    return *std::find(areas.begin(), areas.end(), id);
}

void MainWindow::add_turn(int x, int y) {
    QString current = map_turns[static_cast<std::size_t>(y)][static_cast<std::size_t>(x)]->toPlainText();
    if (++turn < 10) {
        current += '0';
    }
    if (current.split(' ').size() > 9) {
        return;
    }
    current += QString::number(turn) + ' ';
    if ((current.split(' ').size() - 1) % 3 == 0) {
        current += '\n';
    }
    map_turns[static_cast<std::size_t>(y)][static_cast<std::size_t>(x)]->setPlainText(current);
}

void MainWindow::move_player(int x, int y) {
    int id = get_area(map[y][x]).id;
    bool na = false;
    for (unsigned long i = 0; int(i) < int(player->movement_costs.size()); i++) {
        if (player->movement_costs[i].first == id) {
            if (player->movement_costs[i].second < 0) {
                ui->statusBar->showMessage(tr("Invalid move"), 2000);
                na = true;
                break;
            }
        }
    }
    if (!na) {
        player->x = x;
        player->y = y;
        add_turn(x, y);
        player_image->setPos(player->x * cell_width + cell_width * 0.2 / 2, player->y * cell_height + cell_height * 0.2 / 2);
        map.unmask(x, y);
        tree.insertChildren(x, y, player, map, areas); //TODO repair
    }
    on_actionMask_triggered(true);
    if (player->x == map.end.x() && player->y == map.end.y()) {
        ptNode act = tree.findNode(player->x, player->y, tree.root);
        while (act != nullptr && act != tree.root) {
            add_to_path(act->data.x, act->data.y);
            act = act->father;
        }
        QMessageBox::information(this, tr("MapSolver"), QString(tr("¡You win!")), QMessageBox::Ok);
        ui->statusBar->showMessage(tr("WIN, restart the game"));
        set_flags(flags | Flag::Win);
    }
}
void MainWindow::clear() {
    map.clear();
    areas.clear();
    player = nullptr;
    players.clear();

    for (auto row : map_areas) {
        for (auto cell : row) {
            scene->removeItem(cell);
            delete cell;
        }
    }
    map_areas.clear();
    for (auto row : map_turns) {
        for (auto cell : row) {
            scene->removeItem(cell);
            delete cell;
        }
    }
    map_turns.clear();
    if (rect_start) {
        scene->removeItem(rect_start);
        delete rect_start;
        rect_start = nullptr;
    }
    if (rect_end) {
        scene->removeItem(rect_end);
        delete rect_end;
        rect_end = nullptr;
    }
    if (player_image) {
        scene->removeItem(player_image);
        delete player_image;
        player_image = nullptr;
    }

    start_setting = end_setting = false;
    turn = 0;
    set_flags(0x0000);
    reset_ui();
}

void MainWindow::reset_ui() {
    turn = 0;
    turnLabel->clear();
    for (auto row : map_turns) {
        for (auto label : row) {
            label->setPlainText("");
        }
    }
    map.mask_all();
    for (auto row : map_path) {
        for (auto cell : row) {
            cell->setVisible(false);
        }
    }
}

void MainWindow::add_to_path(int pointx, int pointy) {
    map_path[static_cast<unsigned>(pointy)][static_cast<unsigned>(pointx)]->setVisible(true);
}

void MainWindow::on_actionLoad_triggered() {
    QString mapFileString = QFileDialog::getOpenFileName(this, "", "", tr("Text (*.txt)"));
    if (not mapFileString.isEmpty() and mapFileString.endsWith(".txt")) {
        QFile mapFile(mapFileString);
        if (!mapFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QMessageBox::critical(this, tr("MapSolver"), QString(tr("Cannot open the file %1")).arg(mapFileString), QMessageBox::Ok);
            return;
        }
        clear();
        bool aborted = false;

        while (not mapFile.atEnd() and not aborted) {
            QString line = mapFile.readLine();
            auto items = line.left(line.indexOf('\n')).split(',');
            if (items.contains("")) {
                QMessageBox::critical(this, tr("MapSolver"), QString(tr("Extra comma in line %1")).arg(QString::number(map.rows + 1)), QMessageBox::Ok);
                aborted = true;
                break;
            }
            if (not map.cols) {
                map.cols = items.size();
                if (map.cols > MAX_COLS) {
                    QMessageBox::critical(this, tr("MapSolver"), QString(tr("Columns exceeded, maximum %1")).arg(MAX_COLS), QMessageBox::Ok);
                    aborted = true;
                    break;
                }
            } else if (map.cols != items.size()) {
                QMessageBox::critical(this, tr("MapSolver"), QString(tr("Different cols count at line %1; got %2, expected %3")).arg(QString::number(map.rows + 1), QString::number(items.size()), QString::number(map.cols)), QMessageBox::Ok);
                aborted = true;
                break;
            }
            for (int c = 0; c < map.cols; ++c) {
                if (items[c].toInt() < 0) {
                    aborted = true;
                }
                map[map.rows][c] = items[c].toInt();
            }
            ++map.rows;
            if (map.rows > MAX_ROWS) {
                QMessageBox::critical(this, tr("MapSolver"), QString(tr("Rows exceeded, maximum %1")).arg(MAX_ROWS), QMessageBox::Ok);
                aborted = true;
                break;
            }
        }
        if (aborted) {
            clear();
            return;
        }
        set_flags(Flag::MapLoaded);
        on_actionSet_areas_triggered();
    }
}

void MainWindow::on_actionSet_areas_triggered() {
    get_areas();
    area_setter = new AreasSetter(this, &areas);
    if (area_setter->exec() == QDialog::Accepted) {
        render_map();
        set_flags(flags | Flag::AreasSetted);
        on_actionSet_start_triggered();
    }
    delete area_setter;
}

void MainWindow::on_actionSet_start_triggered() {
    start_setting = true;
    ui->statusBar->showMessage("Set start");
}

void MainWindow::on_actionSet_end_triggered() {
    end_setting = true;
    ui->statusBar->showMessage("Set end");
}

void MainWindow::on_actionExit_triggered() {
    qApp->exit();
}

void MainWindow::on_kanbasButton_clicked() {
    if (not flags) {
        return;
    }
    QPoint point = ui->kanbas->mapFromGlobal(QCursor::pos());
    point.rx() /= cell_width;
    point.ry() /= cell_height;
    QString info;
    bool aborted = false;
    if (start_setting) {
        if ((Flag::EndSetted & flags) and map.end.y() == point.y() and map.end.x() == point.x()) {
            info = "Error, Start and end cannot be in the same cell";
            aborted = true;
        } else {
            rect_start->setPos(point.x() * cell_width + 3, point.y() * cell_height + 3);
            map.start = point;
            set_flags(Flag::StartSetted | flags);
            info = "Start established";
            if (not(Flag::EndSetted & flags)) {
                start_setting = false;
                on_actionSet_end_triggered();
                return;
            }
        }
    } else if (end_setting) {
        if ((Flag::StartSetted & flags) and map.start.y() == point.y() and map.start.x() == point.x()) {
            info = "Error, Start and end cannot be in the same cell";
            aborted = true;
        } else {
            rect_end->setPos(point.x() * cell_width + 3, point.y() * cell_height + 3);
            map.end = point;
            set_flags(Flag::EndSetted | flags);
            info = "End established";
        }
    } else { // Cell Info
        info = QString("(%1, %2): %3").arg(QString('A' + point.x()), QString::number(point.y() + 1), get_area(map[point.y()][point.x()]).name);
    }
    if (not aborted) {
        start_setting = end_setting = false;
    }
    ui->statusBar->clearMessage();
    ui->statusBar->showMessage(info, 5000);
}

bool MainWindow::validStart() {
    int id = get_area(map[map.start.y()][map.start.x()]).id;
    for (unsigned long i = 0; int(i) < int(player->movement_costs.size()); i++) {
        if ((player->movement_costs[i].first == id) && int(player->movement_costs[i].second) < 0) {
            QMessageBox msgBox;
            msgBox.setText("¡Error! This player can't be here.");
            msgBox.exec();
            return false;
        }
    }
    return true;
}

void MainWindow::on_actionLoad_player_triggered() {
    player_setter = new PlayerSetter(this, &players, &areas);
    if (player_setter->exec() == QDialog::Accepted) {
        set_flags(flags | Flag::PlayerCreated);
    }
}

void MainWindow::on_actionUp_triggered() {
    set_flags(flags | Flag::PlayingUser);
    if (player->y > 0) {
        move_player(player->x, player->y - 1);
    }
}

void MainWindow::on_actionRight_triggered() {
    set_flags(flags | Flag::PlayingUser);
    if (player->x < map.cols - 1) {
        move_player(player->x + 1, player->y);
    }
}

void MainWindow::on_actionDown_triggered() {
    set_flags(flags | Flag::PlayingUser);
    if (player->y < map.rows - 1) {
        move_player(player->x, player->y + 1);
    }
}

void MainWindow::on_actionLeft_triggered() {
    set_flags(flags | Flag::PlayingUser);
    if (player->x > 0) {
        move_player(player->x - 1, player->y);
    }
}

void MainWindow::on_actionSelect_player_triggered() {
    player_selector = new PlayerSelector(this, &players, &player);
    if (player_selector->exec() == QDialog::Accepted) {
        if (validStart()) {
            map.player = player;
            player_image->setPixmap(player->image.scaled(int(cell_width * 0.8), int(cell_height * 0.8)));
            ui->statusBar->clearMessage();
            set_flags((flags | Flag::PlayerSetted) & ~Flag::Win & ~Flag::PlayingIA & ~Flag::PlayingUser);
            reset_ui();
            move_player(map.start.x(), map.start.y());
            if (ia != nullptr) {
                free(ia);
                ia = nullptr;
            }
        } else {
            player = nullptr;
            QMessageBox::critical(this, tr("MapSolver"), QString(tr("Invalid start")), QMessageBox::Ok);
        }
    }
}

void MainWindow::on_actionMask_triggered(bool masked) {
    for (int r = 0; r < map.rows; ++r) {
        for (int c = 0; c < map.cols; ++c) {
            map_mask[static_cast<std::size_t>(r)][static_cast<std::size_t>(c)]->setVisible(masked and map.masked[r][c]);
        }
    }
}

void MainWindow::on_actionTree_View_triggered() {
    tree_viewer = new TreeViewer(this, tree.root, map_turns);
    tree_viewer->show();
}

void MainWindow::on_actionConfigIA_triggered() {
    auto ia_config = new IAConfig(this, &directions, &repeat_nodes, &manhattan, &algorithm);
    ia_config->exec();
}

void MainWindow::on_actionIA_triggered() {
    if (ia == nullptr) {
        set_flags(flags | Flag::PlayingIA);
        tree.root = nullptr;
        switch (algorithm) {
            case Algorithm::AStar:
                ia = new AStar(&map, true, directions, repeat_nodes, &tree);
                static_cast<AStar*>(ia)->step();
                break;
            case Algorithm::UniformCost:
                ia = new UniformCost(&map, true, directions, repeat_nodes, &tree);
                static_cast<UniformCost*>(ia)->step();
                break;
            case Algorithm::Greedy:
                ia = new Greedy(&map, true, directions, repeat_nodes, &tree);
                static_cast<Greedy*>(ia)->step();
                break;
        }
    }
    try {
        switch (algorithm) {
            case Algorithm::AStar:
                static_cast<AStar*>(ia)->step();
                move_player(static_cast<AStar*>(ia)->current.x(), static_cast<AStar*>(ia)->current.y());
                break;
            case Algorithm::UniformCost:
                static_cast<UniformCost*>(ia)->step();
                move_player(static_cast<UniformCost*>(ia)->current.x(), static_cast<UniformCost*>(ia)->current.y());
                break;
            case Algorithm::Greedy:
                static_cast<Greedy*>(ia)->step();
                move_player(static_cast<Greedy*>(ia)->current.x(), static_cast<Greedy*>(ia)->current.y());
                break;
        }
    } catch (std::logic_error e) {
        QMessageBox::critical(this, tr("MapSolver"), QString(tr(e.what())), QMessageBox::Ok);
    }
}
