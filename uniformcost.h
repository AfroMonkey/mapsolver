#ifndef UNIFORMCOST_H
#define UNIFORMCOST_H

#include "ia.h"

class UniformCost : public IA {
private:
    std::vector<std::pair<QPoint, float>> frontier;
    std::vector<QPoint> closed;
    std::pair<QPoint, float> min();
    int find(QPoint a);

public:
    UniformCost(Map* map, bool m, std::vector<char> directions, bool repeat_nodes, Tree* tree);
    bool step();
};

#endif // UNIFORMCOST_H
