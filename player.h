#ifndef PLAYER_H
#define PLAYER_H

#include <QPixmap>
#include <QString>

class Player {
public:
    QString name;
    QPixmap image;
    std::vector<std::pair<int, float>> movement_costs;
    int x;
    int y;

    Player();
    Player(QString name, QPixmap image, std::vector<std::pair<int, float>> movement_costs) :
      name(name), image(image), movement_costs(movement_costs), x(-1), y(-1) {}
};

#endif // PLAYER_H
