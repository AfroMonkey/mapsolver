#ifndef AREASSETTER_H
#define AREASSETTER_H

#include "area.h"

#include <QDialog>

namespace Ui {
    class AreasSetter;
}

class AreasSetter : public QDialog {
    Q_OBJECT

public:
    explicit AreasSetter(QWidget* parent = nullptr, std::vector<Area>* areas = nullptr);
    ~AreasSetter();

private slots:
    void getColor();
    void done(int r);

private:
    Ui::AreasSetter* ui;
    std::vector<Area>* areas;

    void initialize();
};

#endif // AREASSETTER_H
