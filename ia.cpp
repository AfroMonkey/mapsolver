#include "ia.h"

#include <cmath>

IA::IA(Map* map, bool m, std::vector<char> directions, bool repeat_nodes, Tree* tree) :
  map(map),
  m(m),
  directions(directions),
  repeat_nodes(repeat_nodes),
  tree(tree) {
}

double IA::manhattan(QPoint a) {
    return static_cast<double>(abs(a.x() - map->end.x()) + abs(a.y() - map->end.y()));
}
double IA::euclidean(QPoint a) {
    return sqrt(pow(2, a.x() - map->end.x()) + pow(2, a.y() - map->end.y()));
}
