#ifndef PLAYERSELECTOR_H
#define PLAYERSELECTOR_H

#include "player.h"

#include <QDialog>
#include <QWidget>

namespace Ui {
    class PlayerSelector;
}

class PlayerSelector : public QDialog {
    Q_OBJECT

public:
    explicit PlayerSelector(QWidget* parent = nullptr, std::vector<Player>* players = nullptr, Player** player_index = nullptr);
    ~PlayerSelector();

private slots:
    void done(int r);

private:
    Ui::PlayerSelector* ui;
    std::vector<Player>* players;
    Player** player;
};

#endif // PLAYERSELECTOR_H
