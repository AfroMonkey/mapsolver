#ifndef DATANODE_H
#define DATANODE_H

#include <QVector>

class DataNode {
public:
    int x;
    int y;
    int visit;
    float gn;
    float hn;
    bool closed;

    DataNode() {
        gn = 0;
        hn = 0;
        closed = false;
    }
};

class Node {
public:
    DataNode data;
    Node* father;
    QVector<Node*> children;
};

typedef Node* ptNode;

#endif // DATANODE_H
