#include "greedy.h"

#include <QtDebug>

Greedy::Greedy(Map* map, bool m, std::vector<char> directions, bool repeat_nodes, Tree* tree) :
  IA(map, m, directions, repeat_nodes, tree) {
    q.push_back(map->start);
    tree->insertAlgorithm(map->start, map->start, DataNode());
}

QPoint Greedy::min() {
    auto p = q[0];
    for (auto cell : q) {
        if ((m ? manhattan(cell) : euclidean(cell)) < (m ? manhattan(p) : euclidean(p))) {
            p = cell;
        }
    }
    return p;
}

bool Greedy::step() {
    DataNode data;
    if (not q.size()) {
        throw std::logic_error("Can't reach end");
    }
    current = min();
    q.erase(std::find(q.begin(), q.end(), current));
    closed.push_back(current);
    if (current == map->end) {
        return true;
    }
    for (auto neigbor : map->neighbors(current, directions)) {
        if (repeat_nodes or std::find(closed.begin(), closed.end(), neigbor) == closed.end()) {
            q.push_back(neigbor);
            data.hn = static_cast<float>(m ? manhattan(neigbor) : euclidean(neigbor));
            data.visit = closed.size();
            tree->insertAlgorithm(neigbor, current, data);
        }
    }
    return false;
}
