#ifndef DIRECTIONSPRIORITY_H
#define DIRECTIONSPRIORITY_H

#include <QDialog>

namespace Ui {
    class IAConfig;
}

class IAConfig : public QDialog {
    Q_OBJECT

public:
    explicit IAConfig(QWidget* parent = nullptr, std::vector<char>* directions = nullptr, bool* repeat_nodes = nullptr, bool* manhattan = nullptr, char* algorithm = nullptr);
    ~IAConfig();

private slots:
    void done(int r);
    void on_up_clicked();
    void on_right_clicked();
    void on_down_clicked();
    void on_left_clicked();

private:
    Ui::IAConfig* ui;

    unsigned long i;
    std::vector<char> directions;
    std::vector<char>* directions_remote;
    bool* repeat_nodes;
    bool* manhattan;
    char* algorithm;

    void bc(QPushButton* button);
};

#endif // DIRECTIONSPRIORITY_H
