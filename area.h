#ifndef AREA_H
#define AREA_H

#include <QColor>
#include <QString>
#include <random>

class Area {
public:
    int id;
    QString name;
    QString color;

    Area();
    Area(int id) :
      id(id),
      name("Area " + QString::number(id)),
      color(QColor(random() % 255, random() % 255, random() % 255).name()) {}

    bool operator==(Area a) { return id == a.id; }
    bool operator<(Area a) { return id < a.id; }
    bool operator>(Area a) { return id > a.id; }
};

#endif // AREA_H
