#include "playersetter.h"

#include "areassetter.h"
#include "ui_playersetter.h"

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>

PlayerSetter::PlayerSetter(QWidget* parent, std::vector<Player>* players, std::vector<Area>* areas) :
  QDialog(parent), ui(new Ui::PlayerSetter), players(players) {
    ui->setupUi(this);

    ui->costsTable->setHorizontalHeaderLabels(QStringList({tr("ID"), tr("Color"), tr("Name"), tr("Cost")}));
    ui->costsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    int row = 0;
    for (auto area : *areas) {
        ui->costsTable->insertRow(row);
        // ID
        ui->costsTable->setItem(row, 0, new QTableWidgetItem(QString::number(area.id)));
        ui->costsTable->item(row, 0)->setFlags(ui->costsTable->item(row, 0)->flags() & ~Qt::ItemIsEditable);
        // Color Picker
        ui->costsTable->setItem(row, 1, new QTableWidgetItem());
        ui->costsTable->item(row, 1)->setBackgroundColor(QColor(area.color));
        ui->costsTable->item(row, 1)->setFlags(ui->costsTable->item(row, 0)->flags() & ~Qt::ItemIsEditable);
        //  Name
        ui->costsTable->setItem(row, 2, new QTableWidgetItem(area.name));
        ui->costsTable->item(row, 2)->setFlags(ui->costsTable->item(row, 2)->flags() & ~Qt::ItemIsEditable);
        // Movement costs
        ui->costsTable->setItem(row, 3, new QTableWidgetItem("1"));
        ++row;
    }
}

PlayerSetter::~PlayerSetter() {
    delete ui;
}

void PlayerSetter::on_pushButton_clicked() {
    QString playerFileString = QFileDialog::getOpenFileName(this, "", "", tr("Text (*.png)"));
    ui->imgTextEdit->setText(playerFileString);
}

void PlayerSetter::done(int r) {
    if (QDialog::Accepted == r) {
        if (ui->nameTextEdit->text() == "") {
            QMessageBox::critical(this, tr("MapSolver"), QString(tr("Name required")), QMessageBox::Ok);
            return;
        }
        for (auto player : *players) {
            if (player.name == ui->nameTextEdit->text()) {
                QMessageBox::critical(this, tr("MapSolver"), QString(tr("Player %1 already exists").arg(player.name)), QMessageBox::Ok);
                return;
            }
        }
        if (ui->imgTextEdit->text() == "") {
            QMessageBox::critical(this, tr("MapSolver"), QString(tr("Image required")), QMessageBox::Ok);
            return;
        }
        std::vector<std::pair<int, float>> movement_costs;
        for (int row = 0; row < ui->costsTable->rowCount(); ++row) {
            if (ui->costsTable->item(row, 3)->text() == "NA") {
                ui->costsTable->item(row, 3)->setText("-1");
            }
            bool ok = true;
            float cost = ui->costsTable->item(row, 3)->text().toFloat(&ok);
            if (not ok) {
                QMessageBox::critical(this, tr("MapSolver"), QString(tr("Invalid value %1").arg(ui->costsTable->item(row, 3)->text())), QMessageBox::Ok);
                return;
            }
            cost = std::truncf(cost * 100) / 100;
            movement_costs.push_back(std::pair<int, float>(ui->costsTable->item(row, 0)->text().toInt(), cost));
        }
        players->push_back(Player(ui->nameTextEdit->text(), QPixmap(ui->imgTextEdit->text()), movement_costs));
    }
    QDialog::done(r);
}
