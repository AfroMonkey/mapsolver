#ifndef IA_H
#define IA_H

#include "map.h"
#include "tree.h"

#include <QPoint>
#include <vector>

class IA {
public:
    Map* map;
    bool m;
    std::vector<char> directions;
    bool repeat_nodes;
    Tree* tree;

    double manhattan(QPoint a);
    double euclidean(QPoint a);
    QPoint min();

public:
    QPoint current;
    IA(Map* map, bool m, std::vector<char> directions, bool repeat_nodes, Tree* tree);
};

#endif // IA_H
