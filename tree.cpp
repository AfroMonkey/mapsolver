#include "tree.h"

#include <QDebug>

Tree::Tree() {
    root = nullptr;
}

void Tree::insert(DataNode d, ptNode f) {
    ptNode newNode = new Node();
    newNode->data = d;
    if (root == nullptr) {
        root = newNode;
    } else {
        f->children.append(newNode);
        newNode->father = f;
    }
}

ptNode Tree::findNode(int x, int y, ptNode node) {
    if (node != nullptr) {
        if (node->data.x == x && node->data.y == y) {
            return node;
        } else {
            int j = node->children.size();
            if (j > 0) {
                QVector<Node*>::iterator it = node->children.begin();
                ptNode found = nullptr;
                int i = 0;
                while (i < j && found == nullptr) {
                    found = findNode(x, y, *it);
                    i++;
                    it++;
                }
                return found;
            }
        }
    }
    return nullptr;
}

Area get_area(int id, std::vector<Area> areas) {
    return *std::find(areas.begin(), areas.end(), id);
}

float getCost(Player* player, Map map, int x, int y, std::vector<Area> areas) {
    int id = get_area(map[y][x], areas).id;
    float cost = -1;
    for (unsigned long i = 0; int(i) < int(player->movement_costs.size()); i++) {
        if (player->movement_costs[i].first == id ) {
            if (player->movement_costs[i].second >= 0) {
                cost = player->movement_costs[i].second;
            }
            break;
        }
    }
    return cost;
}

void Tree::insertChildren(int x, int y, Player* player, Map map, std::vector<Area> areas) {
    DataNode childToExpand;
    ptNode parentNode = findNode(x, y, root);
    QVector<std::tuple<int, int, float>> neighbors;

    float cost = getCost(player, map, x, y, areas);
    if (parentNode == nullptr && cost >= 0) {
        childToExpand.x = x;
        childToExpand.y = y;
        childToExpand.gn = 0;
        insert(childToExpand, nullptr);
        parentNode = findNode(x, y, root);
    }

    if (x > 0) neighbors.push_back(std::tuple<int, int, float>(x-1, y, cost));
    if (x < map.cols-1) neighbors.push_back(std::tuple<int, int, float>(x+1, y, cost));
    if (y > 0) neighbors.push_back(std::tuple<int, int, float>(x, y-1, cost));
    if (y < map.rows-1) neighbors.push_back(std::tuple<int, int, float>(x, y+1, cost));

    for(int i = 0; i < neighbors.size(); i++) {
        int xa = std::get<0>(neighbors.at(i));
        int ya = std::get<1>(neighbors.at(i));
        cost = getCost(player, map, xa, ya, areas);
        if (cost >= 0 && findNode(xa, ya, root) == nullptr) {
            childToExpand.x = xa;
            childToExpand.y = ya;
            childToExpand.gn = cost+parentNode->data.gn;
            insert(childToExpand, parentNode);
        }
    }
}

void Tree::insertAlgorithm(QPoint point, QPoint father, DataNode data) {
    if (root == nullptr) {
        insertForAlgorithm(point, nullptr, data);
    } else {
        ptNode f = findNode(father.x(), father.y(), root);
        insertForAlgorithm(point, f, data);
    }
}

void Tree::insertForAlgorithm(QPoint point, ptNode f, DataNode data) {
    ptNode newNode = new Node();
    newNode->data.x = point.x();
    newNode->data.y = point.y();
    newNode->data.gn = data.gn;
    newNode->data.hn = data.hn;
    if (root == nullptr) {
        root = newNode;
        newNode->father = nullptr;
    } else {
        f->children.append(newNode);
        newNode->father = f;
        f->data.visit = data.visit;
        f->data.closed = true;
    }
}

void Tree::changeNode(QPoint point, QPoint newFather, DataNode newData) {
    ptNode child = findNode(point.x(), point.y(), root);
    QVector<Node*>::iterator it = (child->father)->children.begin();
    for (int i = 0; i < (child->father)->children.size(); ++i, it++) {
        if ((*it)->data.x == point.x() && (*it)->data.y == point.y()) {
            (child->father)->children.erase(it);
            break;
        }
    }
    child->data.gn = newData.gn;
    child->data.hn = newData.hn;
    child->father = findNode(newFather.x(), newFather.y(), root);
    (child->father)->children.append(child);
}
