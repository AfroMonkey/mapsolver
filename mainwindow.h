#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "areassetter.h"
#include "astar.h"
#include "greedy.h"
#include "iaconfig.h"
#include "map.h"
#include "playerselector.h"
#include "playersetter.h"
#include "tree.h"
#include "treeviewer.h"
#include "uniformcost.h"

#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QLabel>
#include <QMainWindow>
#include <vector>

namespace Ui {
    class MainWindow;
}

namespace Algorithm {
    enum Algorithm : unsigned char {
        AStar = 'a',
        UniformCost = 'u',
        Greedy = 'g'
    };
}

namespace Flag {
    enum Flag : unsigned int {
        None = 0x0000,
        MapLoaded = 0x0001,
        AreasSetted = 0x0002,
        StartSetted = 0x0004,
        EndSetted = 0x0008,
        PlayerSetted = 0x0010,
        Win = 0x0020,
        PlayerCreated = 0x0040,
        PlayingIA = 0x0080,
        PlayingUser = 0x0100,
    };
}

namespace Direction {

    enum Direction : char {
        Up = 'u',
        Right = 'r',
        Down = 'd',
        Left = 'l'
    };
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionLoad_triggered();
    void on_actionSet_areas_triggered();
    void on_actionSet_start_triggered();
    void on_actionExit_triggered();
    void on_kanbasButton_clicked();
    void on_actionSet_end_triggered();
    void on_actionLoad_player_triggered();
    void on_actionUp_triggered();
    void on_actionRight_triggered();
    void on_actionDown_triggered();
    void on_actionLeft_triggered();
    void on_actionSelect_player_triggered();
    void on_actionMask_triggered(bool checked);
    void on_actionTree_View_triggered();
    void on_actionConfigIA_triggered();
    void on_actionIA_triggered();

private:
    Ui::MainWindow* ui;
    QGraphicsScene* scene;
    QLabel* turnLabel;

    AreasSetter* area_setter;
    PlayerSetter* player_setter;
    PlayerSelector* player_selector;
    TreeViewer* tree_viewer;
    bool repeat_nodes;
    char algorithm;
    void* ia;

    unsigned int flags;
    int MAP_WIDTH;
    int MAP_HEIGHT;
    int cell_width;
    int cell_height;

    Map map;
    std::vector<Area> areas;
    unsigned turn;
    std::vector<Player> players;
    Player* player;
    Tree tree;
    std::vector<char> directions;
    bool manhattan;

    bool start_setting;
    bool end_setting;

    std::vector<std::vector<QGraphicsTextItem*>> map_turns;
    std::vector<std::vector<QGraphicsRectItem*>> map_areas;
    std::vector<std::vector<QGraphicsRectItem*>> map_mask;
    std::vector<std::vector<QGraphicsRectItem*>> map_path;
    QGraphicsRectItem* rect_start;
    QGraphicsRectItem* rect_end;
    QGraphicsPixmapItem* player_image;

    void get_areas();
    bool validStart();
    void prepare_labels();
    void prepare_scene();
    void set_flags(unsigned int flags);
    void render_map();
    Area get_area(int id);
    void add_turn(int x, int y);
    void move_player(int x, int y);
    void clear();
    void reset_ui();
    void add_to_path(int pointx, int pointy);
};

#endif // MAINWINDOW_H
