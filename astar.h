#ifndef ASTAR_H
#define ASTAR_H

#include "ia.h"

class AStar : public IA {
    std::vector<std::pair<QPoint, float>> frontier;
    std::vector<QPoint> closed;
    std::pair<QPoint, float> min();
    int find(QPoint a);

public:
    AStar(Map* map, bool m, std::vector<char> directions, bool repeat_nodes, Tree* tree);
    bool step();
};

#endif // ASTAR_H
